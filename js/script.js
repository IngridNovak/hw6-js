"use strict";
//1.Екранування це спеціальний символ в регулярних виразах.Екранувати можна різного роду символи, наприклад, нам потрібно крапку як розділовий знак, а не спец символ.
//Екранування саме допомогає нам у таких випадках. Використовують зворотній слеш - \. Екранувати можна різного роду спец символи.
//2.Є три способи оголошення функції: function declaration(з допомогою ключового слова function),function expression(функція записується в змінну, не має імені), named function
//expression (функція записується в змінну, але має ім'я).
//3.Hoisting - підняття, це механізм JS при якому змінні і функції ніби рухаються доверху свої області видимості, перед тим як код буде виконаний. Але це стосується лише оголошення
//функції чи змінної, саме значення підняттю не підлягає.
function createNewUser(name,surname){
    let dateString = prompt("Введіть дату народження","dd.mm.yyyy");
    const [day,month,year] = dateString.split('.');
    const birthDate = new Date(+year, month - 1, +day);
    const newUser = {
        birthday: birthDate,
        firstName: name,
        lastName: surname,
        getLogin: function () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        set setFirstName(newFirstName){
            this.firstName = newFirstName;
        },
        set setLastName(newLastName){
            this.lastName = newLastName;
        },
        getAge: function () {
            const today = new Date();
            // const birthDate = new Date(dateString);
            let age = today.getFullYear() - this.birthday.getFullYear();
            const months = today.getMonth() - this.birthday.getMonth();
            if (months < 0 || (months === 0 && today.getDate() < this.birthday.getDate())) {
                age--;
            }
            return age;
        },
        getPassword: function (){
            return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase()) + this.birthday.getFullYear();
 }
            
        };


    return newUser;
};



const fname = prompt('Введіть ім\'я');
const lname = prompt('Введіть прізвище');

const user = createNewUser(fname,lname);


console.log(user);
console.log(user.getLogin());
console.log(user.getAge(user.birthday));
console.log(user.getPassword());


